Source: libaudio-flac-header-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Niko Tyni <ntyni@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               perl-xs-dev,
               perl:native,
               libflac-dev,
               libmodule-install-perl,
               libtest-pod-coverage-perl,
               libtest-pod-perl
Standards-Version: 3.9.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libaudio-flac-header-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libaudio-flac-header-perl.git
Homepage: https://metacpan.org/release/Audio-FLAC-Header
Rules-Requires-Root: no

Package: libaudio-flac-header-perl
Architecture: any
Depends: ${perl:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: Perl interface to FLAC file header metadata
 Audio::FLAC::Header provides an object-oriented interface to FLAC file header
 and metadata, it returns a hash containing basic information about a FLAC
 file, a representation of the embedded cue sheet if one exists, as well as tag
 information contained in the FLAC file's Vorbis tags.
 .
 There is no complete list of tag keys for Vorbis tags, as they can be
 defined by the user; the basic set of tags used for FLAC files include:
 ALBUM, ARTIST, TITLE, DATE, GENRE, TRACKNUMBER, COMMENT.
